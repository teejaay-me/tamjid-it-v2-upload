<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            @if(isset($info))
                <img src="{{url('uploads/user-info/'.$info->avatar)}}" width="48" height="48" alt="User Image"/>
            @endif
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</div>
            <div class="email">{{Auth::user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                    <li role="separator" class="divider"></li>
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>--}}
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>
                            Sign Out
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{Request::is('admin/dashboard')?'active':''}}">
                <a href="{{route('admin.dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
            </li>

            {{--INTRO--}}
            <li class="{{Request::is('admin/intro*')?'active':''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">desktop_mac</i>
                    <span>Site Intro</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{route('intro.index')}}"><i class="material-icons">web</i><span>View Intro</span></a>
                    </li>
                    <li>
                        <a href="{{route('intro.create')}}"><i class="material-icons">web_asset</i><span>Add Intro</span></a>
                    </li>
                </ul>
            </li>

            {{--CATEGORY--}}
            <li class="{{Request::is('admin/category*')?'active':''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">category</i>
                    <span>Category</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{route('category.index')}}"><i class="material-icons">view_list</i><span>All Categories</span></a>
                    </li>
                    <li>
                        <a href="{{route('category.create')}}"><i class="material-icons">add_photo_alternate</i><span>Add Category</span></a>
                    </li>
                </ul>
            </li>

            {{--PROJECT--}}
            <li class="{{Request::is('admin/*project*')?'active':''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">library_books</i>
                    <span>Projects</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{route('project.index')}}"><i class="material-icons">featured_play_list</i><span>All Projects</span></a>
                    </li>
                    <li>
                        <a href="{{route('project.create')}}"><i class="material-icons">note_add</i><span>Add Project</span></a>
                    </li>
                </ul>
            </li>

            {{--USER INFO--}}
            <li class="{{Request::is('admin/user-info*')?'active':''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">person_pin</i>
                    <span>User Info</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{route('user-info.index')}}"><i class="material-icons">person_outline</i><span>All User Info</span></a>
                    </li>
                    <li>
                        <a href="{{route('user-info.create')}}"><i class="material-icons">person_add</i><span>Add User Info</span></a>
                    </li>
                </ul>
            </li>

            {{--SOCIAL LINKS--}}
            <li class="{{Request::is('admin/social-link*')?'active':''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">group</i>
                    <span>Social Links</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{route('social-link.index')}}"><i class="material-icons">format_list_bulleted</i><span>All Social Links</span></a>
                    </li>
                    <li>
                        <a href="{{route('social-link.create')}}"><i class="material-icons">insert_link</i><span>Add Social Link</span></a>
                    </li>
                </ul>
            </li>

            {{--CONTACT--}}
            <li class="{{Request::is('admin/contact')?'active':''}}">
                <a href="{{route('contact.index')}}">
                    <i class="material-icons">contact_mail</i>
                    <span>Messages</span>
                </a>
            </li>


            <li class="header">System</li>
            {{--SETTINGS--}}
            <li class="{{Request::is('admin/settings')?'active':''}}">
                <a href="" class="">
                    <i class="material-icons">settings</i>
                    <span>Settings</span>
                </a>
            </li>
            {{--LOGOUT--}}
            <li class="">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form-2').submit();">
                    <i class="material-icons">input</i><span>{{ __('Logout') }}</span>
                </a>

                <form id="logout-form-2" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>