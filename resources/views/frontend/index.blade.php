<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.partials._header')
</head>
<body>
<!-- Header -->
<header id="header">
    <div class="intro">
        @include('frontend.includes.intro')
    </div>
</header>
<!-- Portfolio Section -->
<div id="portfolio">
    <div class="container">
        @include('frontend.includes.portfolio')
    </div>
</div>
<!-- Contact Section -->
<div id="contact">
    <div class="container">
        <h2>About Me</h2>
        <div class="col-md-6">
            @include('frontend.includes.about')
        </div>
        <div class="col-md-5 col-md-offset-1">
            @include('frontend.includes.contact')
        </div>
    </div>
</div>
@include('frontend.partials._footer')
</body>
</html>
