<div class="overlay">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="intro-text">
                    @if (isset($intro))
                        <h1>{{$intro->first_name}} {{$intro->last_name}}</h1>
                        <p>{{$intro->profession}} / {{$intro->position}}</p>
                    @else
                        <h3>Please enter data from the dashboard</h3>
                    @endif
                    <a href="#portfolio" class="btn btn-custom btn-lg page-scroll">Portfolio</a> <a
                            href="#contact" class="btn btn-custom btn-lg page-scroll">About Me</a></div>
            </div>
        </div>
    </div>
</div>