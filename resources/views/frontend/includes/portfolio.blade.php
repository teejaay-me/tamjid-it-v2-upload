<div class="section-title">
    <h2>Portfolio</h2>
</div>
<div class="categories">
    <ul class="cat">
        <li>
            <ol class="type">
                <li><a href="#" data-filter="*" class="active">All</a></li>
                @if ($categories)
                    @foreach ($categories as $category)
                        <li><a href="#" data-filter=".{{str_slug($category->name)}}">{{title_case($category->name)}}</a></li>
                    @endforeach
                @else
                    <h3>No data found.</h3>
                @endif
            </ol>
        </li>
    </ul>
</div>
<div class="row">
    <div class="portfolio-items">

        @if ($projects)
            @foreach($projects as $project)
                <div class="col-sm-6 col-md-4 {{str_slug($project->category->name)}}">
                    <div class="portfolio-item">
                        <div class="hover-bg"><a href="{{url('uploads/project/'.$project->image)}}" title="{{$project->name}}"
                                                 data-lightbox-gallery="gallery1">
                                <div class="hover-text">
                                    <div class="overlay-caption">
                                        <div class="overlay-content">
                                            <h4>{{$project->name}}</h4>
                                            <h6>{{str_limit($project->description, 45)}}</h6>
                                            <p>click to open in a new tab</p>
                                        </div>
                                    </div>
                                </div>
                                <img src="{{url('uploads/project/'.$project->image)}}" class="img-responsive" alt="{{$project->name}}">
                            </a></div>
                    </div>
                </div>
            @endforeach
        @else
            <h3>No project found</h3>
        @endif

    </div>
</div>