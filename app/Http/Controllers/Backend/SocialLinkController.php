<?php

namespace App\Http\Controllers\Backend;

use App\Models\SocialLink;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socials = SocialLink::all();
        return view('backend.socials.index', compact('socials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.socials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required|url',
            'fa_icon' => 'required',
        ]);
        $social = new SocialLink();
        $social->name = $request->name;
        $social->url = $request->url;
        $social->fa_icon = $request->fa_icon;

        $social->save();

        Toastr::success('Social Link saved successfully!', 'Done');

        return redirect()->route('social-link.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = SocialLink::findOrFail($id);
        return view('backend.socials.edit', compact('social'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required|url',
            'fa_icon' => 'required',
        ]);
        $social = SocialLink::findOrFail($id);

        $social->name = $request->name;
        $social->url = $request->url;
        $social->fa_icon = $request->fa_icon;

        $social->update();

        Toastr::success('Social Link updated successfully!', 'Done');

        return redirect()->route('social-link.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social = SocialLink::findOrFail($id);
        $social->delete();

        Toastr::success('Social Link deleted successfully!', 'Done');
        return redirect()->back();
    }
}
